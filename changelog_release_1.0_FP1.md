CHANGELIST
-


1. To BlockIgnite rule added new option:
    -
    - blockUnderFire - defines block type that should be under the fire to apply the rule (https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Material.html)

Before adding this option there is no way to check for example if obsidian is under the fire or any
other block. Material section of that rule is block type that is "inside" of fire. So if you want for
example to ban obsidian igniting, you should specify:

    material: AIR
    blockUnderFire: OBSIDIAN


2. To EntityDamageByEntity rule added compare, deny and complexItems sections:
    -
    - compare.materials - list of materials of items that need for the rule to be executed (https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Material.html)
    - compare.lore - list of lore of items that need for the rule to be executed
    - compare.enchantments - list of enchants of items that need for the rule to be executed
    - compare.displayName - display name of items that need for the rule to be executed
    - deny.materials - list of materials (any) in the presence of which the rule will not be executed 
    - deny.lore - list of lore strings (any) in the presence of which the rule will not be executed 
    - deny.enchantments - list of enchantments (any) in the presence of which the rule will not be executed 
    - deny.displayName - display name in the presence of which the rule will not be executed
    - complexItems - list of complexItems (of player) that need for the rule to be executed
    
Compare and deny sections works like in BlockBreak rules, so now many EntityDamageByEntity rules can be
executed at one Event. Damage will be summed up from damages of each rule actual for this event.
For example we have 2 rules with such damage sections:

    //rule1//
    damage: 4
    //rule1//
    
    //rule2//
    damage: 10
    //rule2//
    
And for example that two rules are actual for event. So total damage will be 14 from both rules.


3. Added MMSpawn rules to ban mobs from MythicMobs from spawning in regions of WG or on some blocks:
    -
    - name - defines name of rule in scope of all rules (mandatory)
    - blockUnder - defines block under mob while spawning (optional) (Material enum)
    - inRegion - defines is mob spawns in some WG region or no (optional) (true/false, false by default)
    - cancel - defines to cancel event or not (optional)
    
Example 1:

    //START OF FILE//
    ..
    name: preventMMSpawnOnObsidian
    blockUnder: OBSIDIAN
    cancel: true
    ..
    //END OF FILE//
    
Example 2:

    //START OF FILE//
    ..
    name: preventMMSpawnInRegions
    inRegion: true
    cancel: true
    ..
    //END OF FILE//
    
Example 3:

    //START OF FILE//
    ..
    name: preventMMSpawnInRegionsOnObsidian
    blockUnder: OBSIDIAN
    inRegion: true
    cancel: true
    ..
    //END OF FILE//
    
MMSpawn rule is applicable to each Mythic mob.


4. Added new block rule: furnaceSmelt. 
    -
    - name - defines name of rule in scope of all rules (mandatory)
    - cancel - defines to cancel event or not (optional)
    - material - defines type of furnace (mandatory, https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Material.html)
    - source - defines source item in furnace (for ex, IRON_INGOT, mandatory, https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Material.html)
    - result - defines output from smelting (mandatory, single itemMap)
    
After installing new version, folder for that rules will appear
in general block rules folder. Rule can change smelt output of furnace on different source. BUT I DO
RECOMMEND TO USE IT BASED ON RECIPES OF YOUR SERVER. That means, for example in case of vanilla minecraft
where from iron ore we got iron ingot, you should specify in result section of that rule exactly
iron ingot item, not any other. If you specify other item in result, furnace will stuck until player
removes result of smelting (that is default minecraft mechanic of furnace).

Example:

    //START OF FILE//
    name: ironSmeltRule
    material: FURNACE
    source: IRON_ORE
    result: ironForFurnace
    //END OF FILE//
    

5. Added possibility for furnace recipes MODIFICATIONS.
    -
After installing of new version of plugin, in main directory will appear recipe directory and furnace directory,
nested in recipe directory. That directories is used for recipes files. Recipes in AWOP used for now 
only for changing default vanilla recipes, not for adding new. In one FILE of furnace directory there
can be described several recipes. Name of every recipe specified not in name: section, it specified in
head of recipe.

Example:

    //START OF FILE//
    ironSmeltRecipe:
      source: IRON_ORE
      result: ironNuggetForFurnace
    //END OF FILE//

So ironSmeltRecipe is just a name of recipe.

    - source - source of recipe in furnace (mandatory, https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Material.html)
    - result - result of recipe in furnace (single itemMap)
    
IMPORTANT NOTE. For result section of that recipe type (furnace) you can use itemMaps only with
100% chance of item dropping and with equal max and min amount.