CHANGELIST r/1.0_FP2
-


1. Supporting of nested directories in main directories.
    -
    From this update you can make your custom nested directories in every type of block, entity and mm directories, or in
    profile directory, item maps directory, complex items and recipe directories. For example:
    
    AWorldOfPain -> blocks -> break -> yourCustomDirectory1
    
    AWorldOfPain -> complexItems -> yourCustomDirectory2 -> yourCustomDirectory3
    
    AWorldOfPain -> recipe -> yourCustomDirectory4
    
    From all of this custom directories plugin will read all your config files (not depends on level of nesting)
  

2. Supporting of loading names of complex items, item maps, rules and profiles from name of file.
    -
    From this update section "name: someName" is not mandatory anymore. If you will not specify name section, name of
    specific entity will be got from name of file. 
    
    Example (coalOre.yml):
    
        //START OF FILE//
          material: COAL_ORE
          itemMaps:
          - hardcoreCoalFromOreWithLuck1
          defaultDrop: false
          compare:
            materials:
            - DIAMOND_PICKAXE
            - GOLDEN_PICKAXE
            - IRON_PICKAXE
            - STONE_PICKAXE
            - WOODEN_PICKAXE
            - NETHERITE_PICKAXE
            enchantments:
              LOOT_BONUS_BLOCKS: 1
        //END OF FILE//
        
    In this case name of that rule will be same as name of file, except the .yml postfix, so the name will be "coalOre".
    

3. Supporting of direct material section in item maps to avoid of creating simple complex items.
    -
    To specify some item in item map, complex item should be created till current update:
    
    Example of needed complex item:
    
        //START OF FILE//
        material: COAL
        name: coal
        //END OF FILE//
        
    Example of item map:
    
        //START OF FILE//
        name: coalItemMap
        complexItem: coal
        chance: 50
        amount:
          max: 1
          min: 1
        //END OF FILE//
        
    And that format is inconvenient, so from this update you can make same item map different way:
    
    Example of item map (coalItemMap.yml):
    
        //START OF FILE//
        material: COAL
        chance: 50
        amount:
          max: 1
          min: 1
          
    So you can specify just Material enum instead if complex item, also name section is cut off and name of file is used
    from name of file (see the second feature of FP2).
    

4. Supporting of multiple entities per file for rules, item maps and complex items.
    -
    From this update, you can write multiple entities in one .yml file.
    
    Example:
        
        //START OF FILE//
        many: true
        
        hardcoreCoalFromOre:
          material: COAL
          chance: 50
          amount:
            max: 1
            min: 1
            
        hardcoreCoalFromOreWithLuck1:
          material: COAL
          chance: 60
          amount:
            max: 2
            min: 1
            
        hardcoreCoalFromOreWithLuck2:
          material: COAL
          chance: 70
          amount:
            max: 3
            min: 1
            
        hardcoreCoalFromOreWithLuck3:
          material: COAL
          chance: 80
          amount:
            max: 4
            min: 1
        //END OF FILE//
        
    So to specify multiple entities in one file, you need to write "many: true" in file. Also, to specify names of each
    entity in file, you should use headers like names. In example file there will be hardcoreCoalFromOre, 
    hardcoreCoalFromOreWithLuck1, hardcoreCoalFromOreWithLuck2 and hardcoreCoalFromOreWithLuck3 names of entities.
    

5. Added groups to group rules from one file for simple adding all of them under the profile
    - 
    If you will specify some rules in one file, plugin will automatically group them all in one group. That group you
    can use to specify all of rules in file under some profile.
    
    Example (coalOreRules.yml):
    
        //START OF FILE//
        many: true
        
        #COAL_ORE rules
        
        hardcoreCoal:
          material: COAL_ORE
          itemMaps:
          - hardcoreCoalFromOre
          defaultDrop: false
          compare:
            materials:
            - DIAMOND_PICKAXE
            - GOLDEN_PICKAXE
            - IRON_PICKAXE
            - STONE_PICKAXE
            - WOODEN_PICKAXE
            - NETHERITE_PICKAXE
            
            
        hardcoreCoalLuck1:
          material: COAL_ORE
          itemMaps:
          - hardcoreCoalFromOreWithLuck1
          defaultDrop: false
          compare:
            materials:
            - DIAMOND_PICKAXE
            - GOLDEN_PICKAXE
            - IRON_PICKAXE
            - STONE_PICKAXE
            - WOODEN_PICKAXE
            - NETHERITE_PICKAXE
            enchantments:
              LOOT_BONUS_BLOCKS: 1
              
        hardcoreCoalLuck2:
          material: COAL_ORE
          itemMaps:
          - hardcoreCoalFromOreWithLuck2
          defaultDrop: false
          compare:
            materials:
            - DIAMOND_PICKAXE
            - GOLDEN_PICKAXE
            - IRON_PICKAXE
            - STONE_PICKAXE
            - WOODEN_PICKAXE
            - NETHERITE_PICKAXE
            enchantments:
              LOOT_BONUS_BLOCKS: 2
              
        hardcoreCoalLuck3:
          material: COAL_ORE
          itemMaps:
          - hardcoreCoalFromOreWithLuck3
          defaultDrop: false
          compare:
            materials:
            - DIAMOND_PICKAXE
            - GOLDEN_PICKAXE
            - IRON_PICKAXE
            - STONE_PICKAXE
            - WOODEN_PICKAXE
            - NETHERITE_PICKAXE
            enchantments:
              LOOT_BONUS_BLOCKS: 3
        //END OF FILE//
        
    Example of profile:
    
        //START OF FILE//
        name: overworld_profile
        groups:
        - coalOreRules
        //END OF FILE//
        
    So group of some rules in one file will have name of that file without .yml postfix. Name of file in example rules config:
    coalOreRules.yml, so the name of group of that rules will be "coalOreRules". To add all rules in profile, you should add
    name of group in "groups" section like in profile example above.
