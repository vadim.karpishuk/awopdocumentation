CHANGELIST r/1.0_FP3
-


1. Supporting of adding new merchant recipes.
    -
    Now you are able to add completely new recipes to some merchant. All new merchant recipes are in
    AWorldOfPain -> recipe -> merchant -> add directory. Let's see what can be done with that:
   
    Mandatory params:
   
    - source - list with one or two itemMaps which defines items as sources in recipe
    - result - single itemMap which defines item as result of trading
    - maxUses - integer type which defines max uses of recipe until temp blocking
    - chance - double type which defines chance of spawning this recipe in merchant (0.000001-100 in %)

    Optional params:

    - villager.biomeType - biome type of villager (Type enum in Villager class: https://hub.spigotmc.org/javadocs/spigot/org/bukkit/entity/Villager.Type.html)
    - villager.profession - profession of villager (https://hub.spigotmc.org/javadocs/spigot/org/bukkit/entity/Villager.Profession.html)
    - priceMultiplier - honestly, i dont know what it means, but it measured in double and by default is 0
    - exp.toVillager - exp given to merchant after successfull trading, measured in integer (by default it is 1)
    - exp.reward - defines to give exp to player after successfull trading or not (true or false, by default true)

    Example:
   
        //START OF FILE//
        testMerchRecipe:
          source:
          - ironOreMerch
          - coalMerch
          result: ironIngotMerch
          chance: 100
          maxUses: 5
          villager:
            biomeType: JUNGLE
            profession: TOOLSMITH
          exp:
            toVillager: 5
            reward: true
        //END OF FILE//
  

    IMPORTANT NOTES! There is also one more param, which defines chance of replacing "old vanilla" recipe with "new custom".
    This param locates in config.yml and looks like this:

    merchantRecipeReplaceChance: 50 

    Param measured in double from 0.000001-100 in %, by default - 0.
    
    Also, you should notice that new recipes canoot spawn in already filled villagers!


2. Supporting of modifying vanilla merchant recipes.
    -
    You are able to modify vanilla merchant recipes. For example if you want to cut off trading with enchanted books, you can use that new functionality.
    All new merchant recipes are in AWorldOfPain -> recipe -> merchant -> modify directory.
 
    Mandatory params:
    
    - compare.source - param that describes source items of vanilla recipe (list, can contain 1 or 2 itemMaps, should contain at least 1 itemMap)
    - compare.result - param that describes result of trading (single itemMap)
    
    Optional params:
    
    - modify.source - param describes with which items should be replaced old vanilla items in sources of recipe (list, can contain 1 or 2 itemMaps)
    - modify.result - param describes with what item should be replaced old vanilla item in result of trading (single itemMap)
    
    Example:
 
       //START OF FILE//
       farmerCarrot:
          compare:
             source:
             - carrot
             result: emerald
          modify:
             source:
             - carrotModifiedSource
             - bomeMealModifiedFarmer
             result: carrotModifiedResult
       //END OF FILE//


    IMPORTANT NOTE! modify.source and modify.result - at least one of these params should be present in config!


3. Change in food level change rule
    -
    Now field changeType is mandatory (what do that field you can check in release/1.0 doc)


4. Player fish rules added
    - 
    There is new category of rules appeared. New category is applied to player actions. All rules of that category placed in AWorldOfPain -> player directory.
    First rule of this category is about fishing! With that rule you can change what player caught depends on fishing rod, or for example region in which he is.
   
    Optional params:
    
    - defaultDrop - that param defines to drop default vanilla items from caught or not (true/false, true by default if not specified)
    - money.max - max amount of money to give to player (if defined only this section of money, drop of money will work)
    - money.min - min amount of money to give to player(0 by default)
    - money.chance - chance of giving money to player (0.000001-100 in %, by default - 100)
    - compare.materials - list of materials of items that need for the rule to be executed (https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Material.html)
    - compare.lore - list of lore of items that need for the rule to be executed
    - compare.enchantments - list of enchants of items that need for the rule to be executed
    - compare.displayName - display name of items that need for the rule to be executed
    - deny.materials - list of materials (any) in the presence of which the rule will not be executed
    - deny.lore - list of lore strings (any) in the presence of which the rule will not be executed
    - deny.enchantments - list of enchantments (any) in the presence of which the rule will not be executed
    - deny.displayName - display name in the presence of which the rule will not be executed
    - complexItems - list of complexItems (of player) that need for the rule to be executed
    - regions - WG regions where rule can be executed (list)
    - itemMaps - list of itemMaps dropped when rule will be executed
   
    Example:

       //START OF FILE//
       magneticFishing:
         itemMaps:
         - ironMagnetic
         - goldMagnetic 
         defaultDrop: false
         compare:
           materials:
           - FISHING_ROD
           lore:
           - '&1There is a magnet on the hook '
         regions:
         - magn
       //END OF FILE//
        

5. Sounds!!!
    -
    If you have some custom resource pack for your server...you can apply sounds to events through the rules!
    There is a list of rules, where sounds can be applied:
   
      - block.break
      - block.furnaceSmelt
      - block.place
      - entity.breed
      - entity.death
      - entity.foodLevelChange
      - entity.playerDeath
      - entity.shootBow
      - player.fish
   
    To add sounds to rule, you should add some params to rule configs:

    Mandatory params:

    - sound.name - name of sound in resource pack
    - sound.category - category of sound in resource pack
   
    Optional params:

    - sound.volume - volume of sound (double, by default - 100)
    - sound.pitch - pitch of sound (double, by default - 1)
   
    Example:

       //START OF FILE//
       magneticFishing:
         itemMaps:
         - ironMagnetic
         - goldMagnetic
         defaultDrop: false
         compare:
           materials:
           - FISHING_ROD
           lore:
           - '&1There is a magnet on the hook '
         regions:
         - magn
         sound:
           name: custom.debil
           category: MASTER
       //END OF FILE//
        
    Example of sounds.json:
   
       //START OF FILE//
          {
          "custom.arcade": { "category": "master", "sounds": [ "custom/arcade" ] }
          ,
          "custom.song1": { "category": "master", "sounds": [ "custom/song1" ] }
          ,
          "custom.leviosa": { "category": "master", "sounds": [ "custom/leviosa" ] }
          ,
          "custom.debil": { "category": "master", "sounds": [ "custom/debil" ] }
          }
       //END OF FILE//


6. Weather change rule
    -
    There is new category of rules appeared. New category can regulate weather on your server! First rule is about weather change:

    Mandatory params:
   
    - isItRainy - params checks is there should start rain or not (boolean, true/false)
    
    Optional params:
   
    - cancelChance - sets chance of cancelation of changing weather (double, 0.000001-100 in %, by default - 0)
   
    Example:
      
       //START OF FILE//
       isItRainy: true
       cancelChance: 80
       //END OF FILE//

    Rule in example will stop 80% of rains on the server.
