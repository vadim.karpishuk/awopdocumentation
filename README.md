Plugin actually written to perform any changes in vanilla minecraft. You can change loot from bloks depends on item, loot from entities depends on item, exp fallen from exp bottles and etc. Also that all works in different worlds on different rules and can depends from permission of player and 
other aspects.

Available commands:

/awopreload - reloads all config files (awop.reload)

/awopitem [name] - gives custom item in main hand by specified name (awop.item)

There are some objects which are nested in other objects(rules and profiles). ComplexItems and ItemMaps are objects nested in others. First of all, ComplexItem is
just an item with specific lore, enchantments, material and display name.

Mandatory parameters to ComplexItems:

 - name - define complexItem in scope of all complexItems (identifier).
 - material - define material of item (MATERIAL enum of spigot: https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Material.html)
 
Optional parameters to ComplexItems:

 - lore - list of strings in lore
 - enchantments - list of enchants and their levels
 - displayName - display name of an item

    example 1:

        //START OF FILE//
        ..
        name: customIronPickaxe
        material: IRON_PICKAXE
        lore:
        - 'Super duper iron pickaxe'
        enchantments:
          DURABILITY: 3
        displayName: 'Super-Duper-Pickaxe'
        ..
        //END OF FILE//
    
    example 2:

        //START OF FILE//
        ..
        name: iron
        material: IRON_INGOT
        ..
        //END OF FILE//
        
    example 3:

        //START OF FILE//
        ..
        name: chicken
        material: COOKED_CHICKEN
        ..
        //END OF FILE//
        
The definition of ItemMap is to define drop in rules.

Mandatory parameters for ItemMaps:

 - name - define ItemMaps in scope of all itemMaps (identifier)
 - complexItem or mmItem - item from scope of ComplexItems or items from MythicMobs (one of this params should present)
 
Optional parameters for ItemMaps

 - chance - define chance of dropping an item (0.000001-100 in %, by default - 100)
 - amount.max - define max amount of dropped item (1 by default if not specified)
 - amount.min - define min amount of dropped item (1 by default if not specified)
 
    example 1:
    
        //START OF FILE//
        ..
        name: itemMapForMMItem
        mmItem: someMMItemName
        chance: 70.0
        amount:
          max: 10
          min: 2
        ..
        //END OF FILE//
        
    example 2:
    
        //START OF FILE//
        ..
        name: ironMap
        complexItem: iron
        amount:
          max: 2
        ..
        //END OF FILE//
        
Main object in plugin is Rule. Rule is a set of things that can be changed in some Event and it is a set of aspects to define what event should be 
changed. Every Rule matches with specific Event. There is a list of Rule - Event pairs:

block rules:
 -  BREAK - BlockBreakEvent
 -  PLACE - BlockPlaceEvent
 -  FERTILIZE - BlockFertilizeEvent
 -  BURN - BlockBurnEvent
 -  FORM - BlockFormEvent
 -  DAMAGE - BlockDamageEvent
 -  FADE - BlockFadeEvent
 -  IGNITE - BlockIgniteEvent
 -  LEAVES_DECAY - LeavesDecayEvent

entity rules:
 -  CREATURE_SPAWN - CreatureSpawnEvent
 -  BREED - EntityBreedEvent
 -  DAMAGE_BY_BLOCK - EntityDamageByBlockEvent
 -  DAMAGE_BY_ENTITY - EntityDamageByEntityEvent
 -  DEATH - EntityDeathEvent
 -  EXPLODE - EntityExplodeEvent
 -  PICKUP_ITEM - EntityPickupItemEvent
 -  POTION_EFFECT - EntityPotionEffectEvent
 -  SHOOT_BOW - EntityShootBowEvent
 -  TAME - EntityTameEvent
 -  EXP_BOTTLE - ExpBottleEvent
 -  FOOD_LEVEL_CHANGE - FoodLevelChangeEvent
 -  LINGERING_POTION_SPLASH - LingeringPotionSplashEvent
 -  PLAYER_DEATH - PlayerDeathEvent
 -  POTION_SPLASH - PotionSplashEvent

Each config of specific rule type should be in specific directory dedicated to this rule type. Rules have some mandatory and optional parameters in configs.

Mandatory parameters to all rules:

 - name - define rule in scope of all rules (like an identifier)

    example:

        //START OF FILE//
        ..
        name: defaultRuleForSomeEvent
        ..
        //END OF FILE//

Optional parameters to all rules:

 - cancel - define to cancel event or not (true or false)

    example:

        //START OF FILE//
        ..
        cancel: true
        ..
        //END OF FILE//

Mandatory parameters to all block rules:

 - material - define on what block should apply rule (MATERIAL enum of spigot: https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Material.html)

    example:

        //START OF FILE//
        ..
        material: IRON_ORE
        ..
        //END OF FILE//

Mandatory parameters to all entity rules:

 - entity - define on what entity should apply rule (EntityType enum of spigot: https://hub.spigotmc.org/javadocs/spigot/org/bukkit/entity/EntityType.html)

    example:

        //START OF FILE//
        ..
        entity: CREEPER
        ..
        //END OF FILE//

        
Specification of block rules:

 - BREAK:
 
   optional parameters:
   - defaultDrop - define to drop default items from block or not (by vanilla minecraft) (true/false)
   - itemMaps - list of ItemMaps to drop from block
   - complexItems - list of complexItems (of player) that need to break the block for the rule to be executed
   - money.max - max amount of money to give to player by breaking the block (if defined only this section of money, drop of money will work)
   - money.min - min amount of money to give to player by breaking the block (0 by default)
   - money.chance - chance of giving money to player (0.000001-100 in %, by default - 100)
   - compare.materials - list of materials of items that need to break the block for the rule to be executed (https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Material.html)
   - compare.lore - list of lore of items that need to break the block for the rule to be executed
   - compare.enchantments - list of enchants of items that need to break the block for the rule to be executed
   - compare.displayName - display name of items that need to break the block for the rule to be executed
   - deny.materials - list of materials (any) in the presence of which the rule will not be executed 
   - deny.lore - list of lore strings (any) in the presence of which the rule will not be executed 
   - deny.enchantments - list of enchantments (any) in the presence of which the rule will not be executed 
   - deny.displayName - display name in the presence of which the rule will not be executed 
   - permission - permission that needs player for the rule to be executed
   
        example:
     
            //START OF FILE//
            ..
            name: defaultIronOreRule
            material: IRON_ORE
            permission: master
            defaultDrop: false
            itemMaps:
            - ironMap
            money:
              max: 10
            compare:
              material:
              - DIAMOND_PICKAXE
              lore:
              - 'Fires everything!'
              displayName: 'Fired pickaxe'
              enchantments:
                DIG_SPEED: 5
            ..
            //END OF FILE// 
            
 - BURN:
 
   optional parameters:
   - replaceMaterial - define material which will replace the old one ()
   - itemMaps - list of ItemMaps to drop from block
   
        example:
     
            //START OF FILE//
            ..
            name: defaultBurnRule
            material: BIRCH_WOOD
            replaceMaterial: COAL_BLOCK
            itemMaps:
            - ironMap
            ..
            //END OF FILE// 
            
 - DAMAGE:
 
   optional parameters:
   - complexItems - list of complexItems (of player) that need to damage the block for the rule to be executed
   - compare.materials - list of materials of items that need to damage the block for the rule to be executed (https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Material.html)
   - compare.lore - list of lore of items that need to damage the block for the rule to be executed
   - compare.enchantments - list of enchants of items that need to damage the block for the rule to be executed
   - compare.displayName - display name of items that need to damage the block for the rule to be executed
   - deny.materials - list of materials (any) in the presence of which the rule will not be executed 
   - deny.lore - list of lore strings (any) in the presence of which the rule will not be executed 
   - deny.enchantments - list of enchantments (any) in the presence of which the rule will not be executed 
   - deny.displayName - display name in the presence of which the rule will not be executed 
   - permission - permission that needs player for the rule to be executed
   - instantBreak - to perform instant break or not (true/false)
   
        example:
     
            //START OF FILE//
            ..
            name: defaultDamageRule
            material: IRON_ORE
            permission: master
            compare:
              material:
              - DIAMOND_PICKAXE
              lore:
              - 'Fires everything!'
              displayName: 'Fired pickaxe'
              enchantments:
                DIG_SPEED: 5
            instantBreak: true
            ..
            //END OF FILE//
            
 - FADE:
 
   optional parameters:
   - itemMaps - list of ItemMaps to drop from block
   
        example:
     
            //START OF FILE//
            ..
            name: defaultFadeRule
            material: ICE
            itemMaps:
            - ironMap
            ..
            //END OF FILE//
            
 - FERTILIZE:
 
   optional parameters:
   - growth.chance - chance of growing seeds for some levels (0.000001-100 in %, by default - 100)
   - growth.stages - stages to grow up (0-7, 2 by default)
   - permission - permission that needs player for the rule to be executed
   
        example:
     
            //START OF FILE//
            ..
            name: defaultFertilizeRule
            material: WHEAT
            permission: master
            growth:
              stages: 7
              chance: 100.0
            ..
            //END OF FILE//
            
 - FORM:
 
   optional parameters:
   - replaceMaterial - define material which will replace the old one
   
        example:
     
            //START OF FILE//
            ..
            name: defaultFormRule
            material: OBSIDIAN
            replaceMaterial: COBBLESTONE
            ..
            //END OF FILE//
            
 - IGNITE:
 
   optional parameters:
   - igniteCause - define cause of ignition (IgniteCause enum of Spigot: https://hub.spigotmc.org/javadocs/bukkit/org/bukkit/event/block/BlockIgniteEvent.IgniteCause.html)
   - permission - permission that needs player for the rule to be executed (will apply ONLY for FLINT_AND_STEEL ignite cause)
   
        example:
     
            //START OF FILE//
            ..
            name: defaultIgniteRule
            material: OBSIDIAN
            cancel: true
            igniteCause: FLINT_AND_STEEL
            permission: noob
            ..
            //END OF FILE//
            
 - LEAVES_DECAY:
 
   optional parameters:
   - itemMaps - list of ItemMaps to drop from block
   
        example:
     
            //START OF FILE//
            ..
            name: defaultLeavesDecayRule
            material: DARK_OAK_LEAVES
            itemMaps:
            - ironMap
            ..
            //END OF FILE//
            
 - PLACE:
 
   optional parameters:
   - permission - permission that needs player for the rule to be executed
   
        example:
     
            //START OF FILE//
            ..
            name: defaultPlaceRule
            material: OBSIDIAN
            cancel: true
            permission: noob
            ..
            //END OF FILE//
            
Specification of block rules:

 - CREATURE_SPAWN:
 
   optional parameters:
   - itemMaps - list of itemMaps to drop under mob
   - cancelChance - defines chance of spawn cancellation (0.000001-100 in %, by default - 0)
   - spawnReason - defines the reason of spawning mob (Spigot enum: https://hub.spigotmc.org/javadocs/bukkit/org/bukkit/event/entity/CreatureSpawnEvent.SpawnReason.html)
   
        example: 
        
            //START OF FILE//
            ..
            name: defaultCreatureSpawn
            entity: SKELETON
            cancelChance: 50.5
            itemMaps:
            - ironMap
            spawnReason: SPAWNER
            ..
            //END OF FILE//
            
 - BREED:
 
   optional parameters:
   - itemMaps - list of itemMaps to drop under mob
   - cancelChance - defines chance of breeding cancellation (0.000001-100 in %, by default - 0)
   - experience - defines experience falling from entity (integer)
   - permission - permission that needs player for the rule to be executed
   - money.max - max amount of money to give to player from breeding (if defined only this section of money, drop of money will work)
   - money.min - min amount of money to give to player from breeding (0 by default)
   - money.chance - chance of giving money to player (0-100) (100 by default)
   
        example: 
        
            //START OF FILE//
            ..
            name: defaultBreedRule
            entity: COW
            cancelChance: 50.5
            itemMaps:
            - ironMap
            ..
            //END OF FILE//
            
 - DAMAGE_BY_BLOCK:
 
   optional parameters:
   - damage - what damage will be set (can be dotted number)
   - material - defines from what block will give damage (MATERIAL enum: https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Material.html)
   
        example: 
        
            //START OF FILE//
            ..
            name: defaultEntityDamageByBlockRule
            entity: COW
            damage: 2.5
            material: CACTUS
            ..
            //END OF FILE//
            
 - DAMAGE_BY_ENTITY:
 
   optional parameters:
   - damage - what damage will be set (can be dotted number)
   - attackerType - defines from what entity will give damage (EntityType enum: https://hub.spigotmc.org/javadocs/spigot/org/bukkit/entity/EntityType.html)
   - permission - permission that needs player for the rule to be executed (applied if player is attacker in event and does not depend from attackerType)
   
        example: 
        
            //START OF FILE//
            ..
            name: defaultEntityDamageByEntityRule
            entity: COW
            damage: 2
            attackerType: PLAYER
            ..
            //END OF FILE//
            
 - DEATH:
 
   optional parameters:
   - experience - defines experience falling from entity (integer)
   - itemMaps - list of itemMaps to drop under mob
   - defaultDrop - true value used to drop default vanilla items, false to disable drop of default vanilla items (true/false, true by default) 
   
        example: 
        
            //START OF FILE//
            ..
            name: defaultDeathRule
            entity: COW
            experience: 20
            defaultDrop: false
            itemMaps:
            - ironMap
            ..
            //END OF FILE//
            
 - EXPLODE:
 
   optional parameters:
   - yield - defines percentage of blocks to drop from this explosion (floating point number, where 1.0 is 100%)
   
        example: 
        
            //START OF FILE//
            ..
            name: defaultExplodeRule
            entity: CREEPER
            yield: 1.0
            ..
            //END OF FILE//
            
 - PICKUP_ITEM:
 
   optional parameters:
   no params
   
        example: 
        
            //START OF FILE//
            ..
            name: defaultPickupItemRule
            entity: ZOMBIE
            cancel: true
            ..
            //END OF FILE//
            
 - POTION_EFFECT:
 
   optional parameters:
   no params
   
        example: 
        
            //START OF FILE//
            ..
            name: defaultPotionEffectRule
            entity: ZOMBIE
            cancel: true
            ..
            //END OF FILE//
            
 - SHOOT_BOW:
 
   optional parameters:
   - projectile - with that arrow will be replaced (EntityType enum: https://hub.spigotmc.org/javadocs/spigot/org/bukkit/entity/EntityType.html)
   - compare.materials - list of materials of items that need for the rule to be executed (https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Material.html)
   - compare.lore - list of lore of items that need for the rule to be executed
   - compare.enchantments - list of enchants of items that need for the rule to be executed
   - compare.displayName - display name of items that need for the rule to be executed
   - deny.materials - list of materials (any) in the presence of which the rule will not be executed 
   - deny.lore - list of lore strings (any) in the presence of which the rule will not be executed 
   - deny.enchantments - list of enchantments (any) in the presence of which the rule will not be executed 
   - deny.displayName - display name in the presence of which the rule will not be executed 
   - complexItems - list of complexItems (of player) that need for the rule to be executed
   
        example: 
        
            //START OF FILE//
            ..
            name: defaultShootBowRule
            entity: PLAYER
            projectile: SMALL_FIREBALL
            ..
            //END OF FILE//
            
 - TAME:
 
   optional parameters:
   no params
   
        example: 
        
            //START OF FILE//
            ..
            name: defaultTameRule
            entity: WOLF
            cancel: true
            ..
            //END OF FILE//
            
 - EXP_BOTTLE:
 
   optional parameters:
   - experience.max - max value of experience will fall (integer)
   - experience.min - min value of experience will fall (integer)
   - itemMaps - list of itemMaps to drop
   
        example: 
        
            //START OF FILE//
            ..
            name: defaultExpBottleRule
            entity: THROWN_EXP_BOTTLE
            experience:
              max: 20
              min: 5
            ..
            //END OF FILE//
            
 - FOOD_LEVEL_CHANGE:
 
   optional parameters:
   - permission - permission that needs player for the rule to be executed
   - foodLevel - defines what level should set because of this rule (integer)
   - changeType - defines to INCREASE or DECREASE current food level of player (INCREASE/DECREASE)
   - complexItem - defines complexItem that player needs to consume to perform this rule
   
        example: 
        
            //START OF FILE//
            ..
            name: defaultFoodLevelChangeRule
            entity: PLAYER
            foodLevel: 5
            changeType: INCREASE
            complexItem: chicken
            ..
            //END OF FILE//
            
 - LINGERING_POTION_SPLASH:
 
   optional parameters:
   no params
   
        example: 
        
            //START OF FILE//
            ..
            name: defaultLingeringPotionSplashRule
            entity: SPLASH_POTION
            cancel: true
            ..
            //END OF FILE//
            
 - PLAYER_DEATH:
 
   optional parameters:
   - keep.experience - defines to keep experience or no (true/false)
   - keep.inventory - defines to keep inventory or no (true/false)
   
        example: 
        
            //START OF FILE//
            ..
            name: defaultPlayerDeathRule
            entity: PLAYER
            keep:
              experience: true
              inventory: true
            ..
            //END OF FILE//
            
 - POTION_SPLASH:
 
   optional parameters:
   no params
   
        example: 
        
            //START OF FILE//
            ..
            name: defaultPotionSplashRule
            entity: SPLASH_POTION
            cancel: true
            ..
            //END OF FILE//
            
To apply rules to some world, you should form profiles. Profile - is a combination of some rules:

Mandatory parameters to profile:

 - name - define profile in scope of all profiles (identifier)
 
Optional parameters to profile:
 
 - rule.block.break
 - rule.block.burn
 - rule.block.damage
 - rule.block.fade
 - rule.block.fertilize
 - rule.block.form
 - rule.block.ignite
 - rule.block.leavesDecay
 - rule.block.place
 - rule.entity.breed
 - rule.entity.damageByBlock
 - rule.entity.damageByEntity
 - rule.entity.death
 - rule.entity.expBottle
 - rule.entity.explode
 - rule.entity.foodLevelChange
 - rule.entity.lingeringPotionSplash
 - rule.entity.pickupItem
 - rule.entity.playerDeath
 - rule.entity.potionEffect
 - rule.entity.potionSplash
 - rule.entity.creatureSpawn
 - rule.entity.shootBow
 - rule.entity.tame
 
    example:
    
        //START OF FILE//
        ..
        name: defaultProfile
        rule:
          block:
            damage:
            - defaultDamageRule
            fade:
            - defaultFadeRule
            fertilize:
            - defaultFertilizeRule
        ..
        //END OF FILE//
        
To enable your profile, you should apply it to some world or worlds. To do it, make some changes in main config.yml:

   example:
    
        //START OF FILE//
        ..
        activeProfiles:
          world: basicProfile
          world_nether: netherProfile
        ..
        //END OF FILE//
        

SOME IMPORTANT NOTES


NOTE 1:

In some rules you can see compare. and deny. sections. With that sections you can create rules hierarchically. That means
you can create gradations of rules. For example, you want to do rule to drop already melted ores. BUT! You always want 
this to work with the enchantment of LOOT_BONUS_BLOCKS on different levels. Needed configs for that are given below (BREAK rule): 

    //START OF FILE//
    ..
    name: firedPickaxeRule
    defaultDrop: false
    material: IRON_ORE
    compare:
      materials:
      - WOODEN_PICKAXE
      - STONE_PICKAXE
      - IRON_PICKAXE
      - GOLDEN_PICKAXE
      - DIAMOND_PICKAXE
      - NETHERITE_PICKAXE
      lore:
      - '&4Ready to fire anything!!!'
    itemMaps:
    - ironMap1
    ..
    //END OF FILE//
    

    //START OF FILE//
    ..
    name: firedPickaxeRuleLoot1
    defaultDrop: false
    material: IRON_ORE
    compare:
      materials:
      - WOODEN_PICKAXE
      - STONE_PICKAXE
      - IRON_PICKAXE
      - GOLDEN_PICKAXE
      - DIAMOND_PICKAXE
      - NETHERITE_PICKAXE
      lore:
      - '&4Ready to fire anything!!!'
      enchantments:
        LOOT_BONUS_BLOCKS: 1
    itemMaps:
    - ironMap2
    ..
    //END OF FILE//
    
    
    //START OF FILE//
    ..
    name: firedPickaxeRuleLoot2
    defaultDrop: false
    material: IRON_ORE
    compare:
      materials:
      - WOODEN_PICKAXE
      - STONE_PICKAXE
      - IRON_PICKAXE
      - GOLDEN_PICKAXE
      - DIAMOND_PICKAXE
      - NETHERITE_PICKAXE
      lore:
      - '&4Ready to fire anything!!!'
      enchantments:
        LOOT_BONUS_BLOCKS: 2
    itemMaps:
    - ironMap3
    ..
    //END OF FILE//
      
    
    //START OF FILE//
    ..
    name: firedPickaxeRuleLoot3
    defaultDrop: false
    material: IRON_ORE
    compare:
      materials:
      - WOODEN_PICKAXE
      - STONE_PICKAXE
      - IRON_PICKAXE
      - GOLDEN_PICKAXE
      - DIAMOND_PICKAXE
      - NETHERITE_PICKAXE
      lore:
      - '&4Ready to fire anything!!!'
      enchantments:
        LOOT_BONUS_BLOCKS: 3
    itemMaps:
    - ironMap4
    ..
    //END OF FILE//
    
Soooo as you see there are rules which differ in LOOT_BONUS_BLOCKS enchantment. So for example if player have pickaxe with
'&4Ready to fire anything!!!' lore and LOOT_BONUS_BLOCKS: 3 enchantment, only firedPickaxeRuleLoot3 will be executed. Algorithm
finds nested rules in other rules by compare. section and throws out of execution nested rules.

Deny section works slightly different. If item has some (any) option specified in deny section, rule will not be executed. Simple.


NOTE 2:

Some events can be changed by multiple rules, and some events can be changed only by one selected rule.
Event-rule pairs with multiple rules execution:

   -  BREAK - BlockBreakEvent
   -  DAMAGE - BlockDamageEvent
   -  SHOOT_BOW - EntityShootBowEvent
     
That means for example if you have two block rule break rules with DIFFERENT specified compare.lore sections and if player
has item with BOTH specified compare.lore sections, that two rules will be executed for this event.
