CHANGELIST r/1.0_FP4
-


1. New rule to block operations added: BlockRuleExp
    -
    New rule can modify experience fallen from block, for ex. diamond ore, coal, or some other where
    exp fallen by default vanilla minecraft.

   Mandatory params:

   - material - define on what block should apply rule (MATERIAL enum of spigot: https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Material.html)
   - expToDrop - define amount of exp fallen from that block (integer)

   Example:

        //START OF FILE//
        many: true
        
        testExpRule:
          material: DIAMOND_ORE
          expToDrop: 20
        //END OF FILE//

   Test rule will drop 20 exp from diamond ore.


2. Sounds now are tied to itemMaps too. 
    -
    That means you can add some sounds while some item from item map drops. Also old sounds tied to rule execution
    are working.
   
    Mandatory params:

    - sound.name - name of sound in resource pack
    - sound.category - category of sound in resource pack

    Optional params:

    - sound.volume - volume of sound (double, by default - 100)
    - sound.pitch - pitch of sound (double, by default - 1)
   
    Example (some itemMap):
   
        //START OF FILE//
        name: meteoricForCoal
        mmItem: Meteoric_iron
        chance: 100
        sound:
          name: block.anvil.destroy
          category: MASTER
        //END OF FILE//

    If item will drop from some rule execution, sound will be played.
   

3. New rule: PlayerShearRule. New rule applied to player actions of shearing sheeps or for example mushroom cows.
    -
    With that rule you can add for example some items to drop while player shears sheepls or mushroom cows. Also
    you can specify color of sheep or cow to execute rule. Or even more you can add money to add to player account.
   
    Mandatory params:

    - entity - define on what entity should apply rule (MUSHROOM_COW or SHEEP)
    
    Optional params:

    - color - define on what color of entity should apply rule (DyeColor enum for SHEEP: https://hub.spigotmc.org/javadocs/spigot/org/bukkit/DyeColor.html ; MushroomCow.Variant enum for MUSHROOM_COW: https://hub.spigotmc.org/javadocs/spigot/org/bukkit/entity/MushroomCow.Variant.html)
    - money.max - max amount of money to give to player (if defined only this section of money, drop of money will work)
    - money.min - min amount of money to give to player(0 by default)
    - money.chance - chance of giving money to player (0.000001-100 in %, by default - 100)
    - sound.name - name of sound in resource pack or default minecraft (mandatory to specify sound)
    - sound.category - category of sound in resource pack or default minecraft (mandatory to specify sound)
    - sound.volume - volume of sound (double, by default - 100) (optional to specify sound)
    - sound.pitch - pitch of sound (double, by default - 1) (optional to specify sound)
    - itemMaps - list of itemMaps dropped when rule will be executed
    - compare.materials - list of materials of items that needed to shear entity for the rule to be executed (https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Material.html)
    - compare.lore - list of lore of items that needed to shear entity for the rule to be executed
    - compare.enchantments - list of enchants of items that needed to shear entity for the rule to be executed
    - compare.displayName - display name of items that needed to shear entity for the rule to be executed
    - deny.materials - list of materials (any) in the presence of which the rule will not be executed
    - deny.lore - list of lore strings (any) in the presence of which the rule will not be executed
    - deny.enchantments - list of enchantments (any) in the presence of which the rule will not be executed
    - deny.displayName - display name in the presence of which the rule will not be executed
   
    Also there are some other options to specify armor of player like you can specify item, but i will describe that
    functionality in next paragraph of changelist.
   
    Example:

        //START OF FILE//
        many: true
        
        testShearRule:
          entity: SHEEP
          color: YELLOW
          itemMaps:
          - fluxMapGold
        //END OF FILE//

    Example rule can drop item in fluxMapGold itemMap if player will shear yellow sheep.


4. You can specify some armor properties in some rule to execute it or not.
    -
    Like you can specify some item on some rule, now you can specify armor of player the same way. Also you can
    build up trees of effects based on combinations of different armor elements and item used by player.]
    List of rules where you can specify armor:
    
      - block.break
      - player.shear
 
    All params are optional:
 
    - helmet.compare.materials - list of materials in helmet slot that needed for the rule to be executed (https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Material.html)
    - helmet.compare.lore - list of lore in helmet slot that needed for the rule to be executed
    - helmet.compare.enchantments - list of enchants in helmet slot that needed for the rule to be executed
    - helmet.compare.displayName - display name of helmet slot that needed for the rule to be executed
    - helmet.deny.materials - list of materials (any) in the presence of which in helmet slot the rule will not be executed
    - helmet.deny.lore - list of lore strings (any) in the presence of which in helmet slot the rule will not be executed
    - helmet.deny.enchantments - list of enchantments (any) in the presence of which in helmet slot the rule will not be executed
    - helmet.deny.displayName - display name in the presence of which in helmet slot the rule will not be executed
 
    - chestPlate.compare.materials - list of materials in chestPlate slot that needed for the rule to be executed (https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Material.html)
    - chestPlate.compare.lore - list of lore in chestPlate slot that needed for the rule to be executed
    - chestPlate.compare.enchantments - list of enchants in chestPlate slot that needed for the rule to be executed
    - chestPlate.compare.displayName - display name of chestPlate slot that needed for the rule to be executed
    - chestPlate.deny.materials - list of materials (any) in the presence of which in chestPlate slot the rule will not be executed
    - chestPlate.deny.lore - list of lore strings (any) in the presence of which in chestPlate slot the rule will not be executed
    - chestPlate.deny.enchantments - list of enchantments (any) in the presence of which in chestPlate slot the rule will not be executed
    - chestPlate.deny.displayName - display name in the presence of which in chestPlate slot the rule will not be executed
 
    - leggings.compare.materials - list of materials in leggings slot that needed for the rule to be executed (https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Material.html)
    - leggings.compare.lore - list of lore in leggings slot that needed for the rule to be executed
    - leggings.compare.enchantments - list of enchants in leggings slot that needed for the rule to be executed
    - leggings.compare.displayName - display name of leggings slot that needed for the rule to be executed
    - leggings.deny.materials - list of materials (any) in the presence of which in leggings slot the rule will not be executed
    - leggings.deny.lore - list of lore strings (any) in the presence of which in leggings slot the rule will not be executed
    - leggings.deny.enchantments - list of enchantments (any) in the presence of which in leggings slot the rule will not be executed
    - leggings.deny.displayName - display name in the presence of which in leggings slot the rule will not be executed
 
    - boots.compare.materials - list of materials in boots slot that needed for the rule to be executed (https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Material.html)
    - boots.compare.lore - list of lore in boots slot that needed for the rule to be executed
    - boots.compare.enchantments - list of enchants in boots slot that needed for the rule to be executed
    - boots.compare.displayName - display name of boots slot that needed for the rule to be executed
    - boots.deny.materials - list of materials (any) in the presence of which in boots slot the rule will not be executed
    - boots.deny.lore - list of lore strings (any) in the presence of which in boots slot the rule will not be executed
    - boots.deny.enchantments - list of enchantments (any) in the presence of which in boots slot the rule will not be executed
    - boots.deny.displayName - display name in the presence of which in boots slot the rule will not be executed
   
    Example (block break rules):

        //START OF FILE//
        many: true
  
        meteoricRuleIron:
          material: IRON_ORE
          defaultDrop: true
          itemMaps:
          - meteoricForIron
          - fluxMapIron
          compare:
            materials:
            - STONE_PICKAXE
            - IRON_PICKAXE
            - GOLDEN_PICKAXE
            - DIAMOND_PICKAXE
            - NETHERITE_PICKAXE
          deny:
            enchantments:
              SILK_TOUCH: 1
          chestPlate:
            compare:
              lore:
              - '&2first_line'
              - '&2second_line'
              - '&cthird_line'
          leggings:
            compare:
              lore:
              - '&2fourth_line'
              - '&2fifth_line'
              - '&csix_line'
        
        meteoricRuleIronGoldFlux:
        material: IRON_ORE
        defaultDrop: true
        itemMaps:
        - meteoricForIron
        - fluxMapGold
        compare:
          materials:
          - STONE_PICKAXE
          - IRON_PICKAXE
          - GOLDEN_PICKAXE
          - DIAMOND_PICKAXE
          - NETHERITE_PICKAXE
        deny:
          enchantments:
            SILK_TOUCH: 1
        chestPlate:
          compare:
            lore:
            - '&2first_line'
            - '&2second_line'
            - '&cthird_line'
        leggings:
          compare:
            lore:
            - '&2fourth_line'
            - '&2fifth_line'
            - '&csix_line'
        helmet:
          compare:
            lore:
            - '&2seven'
            - '&2¯\_(ツ)_/¯'
            - '&ceight'
            - '&cnine'
            - '&cten'
        //END OF FILE//

    In example file a simple tree of two elements will be formed. And depends on armor of player will be executed first
    or second rule.
