CHANGELIST r/1.0_FP5
-


1. More rules now can be armor-dependent.
    -
    Now you can specify armor in some rule to decide, can it be executed if a player for example
    will have diamond boots in armor slot. Or you can ban for example Protection 4 enchantment
    on all armor pieces, so some rule will not be executed. Also, you should know that armor filter
    will work like old item in hand filter (like in block break rule, or player fish rule). That means
    you can build up trees of effects based on the difference in lore, enchantments, material, 
    and so on between each such specification within the same rule type. ALSO, YOU SHOULD NOTICE THAT 
    NOW ITEM IN HAND SPECIFICATION IS UNITED WITH ARMOR!!! IT MEANS, TREES OF EFFECTS WILL BE BASED ON
    ARMOR AND ITEM IN HAND SIMULTANEOUSLY. Let's see possible fields of such specification:
   
    Optional params:
    - helmet.compare.materials - list of materials of helmet that needed for the rule to be executed (https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Material.html)
    - helmet.compare.lore - list of lore of helmet that needed for the rule to be executed
    - helmet.compare.enchantments - list of enchants of helmet that needed for the rule to be executed
    - helmet.compare.displayName - display name of helmet that needed for the rule to be executed
    - helmet.deny.materials - list of materials (any) in the presence of which the rule will not be executed
    - helmet.deny.lore - list of lore strings (any) in the presence of which the rule will not be executed
    - helmet.deny.enchantments - list of enchantments (any) in the presence of which the rule will not be executed
    - helmet.deny.displayName - display name in the presence of which the rule will not be executed
    - chestPlate.compare.materials - list of materials of chestPlate that needed for the rule to be executed (https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Material.html)
    - chestPlate.compare.lore - list of lore of chestPlate that needed for the rule to be executed
    - chestPlate.compare.enchantments - list of enchants of chestPlate that needed for the rule to be executed
    - chestPlate.compare.displayName - display name of chestPlate that needed for the rule to be executed
    - chestPlate.deny.materials - list of materials (any) in the presence of which the rule will not be executed
    - chestPlate.deny.lore - list of lore strings (any) in the presence of which the rule will not be executed
    - chestPlate.deny.enchantments - list of enchantments (any) in the presence of which the rule will not be executed
    - chestPlate.deny.displayName - display name in the presence of which the rule will not be executed
    - leggings.compare.materials - list of materials of leggings that needed for the rule to be executed (https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Material.html)
    - leggings.compare.lore - list of lore of leggings that needed for the rule to be executed
    - leggings.compare.enchantments - list of enchants of leggings that needed for the rule to be executed
    - leggings.compare.displayName - display name of leggings that needed for the rule to be executed
    - leggings.deny.materials - list of materials (any) in the presence of which the rule will not be executed
    - leggings.deny.lore - list of lore strings (any) in the presence of which the rule will not be executed
    - leggings.deny.enchantments - list of enchantments (any) in the presence of which the rule will not be executed
    - leggings.deny.displayName - display name in the presence of which the rule will not be executed
    - boots.compare.materials - list of materials of boots that needed for the rule to be executed (https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Material.html)
    - boots.compare.lore - list of lore of boots that needed for the rule to be executed
    - boots.compare.enchantments - list of enchants of boots that needed for the rule to be executed
    - boots.compare.displayName - display name of boots that needed for the rule to be executed
    - boots.deny.materials - list of materials (any) in the presence of which the rule will not be executed
    - boots.deny.lore - list of lore strings (any) in the presence of which the rule will not be executed
    - boots.deny.enchantments - list of enchantments (any) in the presence of which the rule will not be executed
    - boots.deny.displayName - display name in the presence of which the rule will not be executed
    
    So as you see, the set of params is quite similar to old item in hand specification which was previously
    used in block rule break, player fish rules and other. BUT DO NOT BE AFRAID, THE DEFINITION OF ITEM IN HAND
    SPECIFICATION HASN'T CHANGED!!! Let's see some example to see how you can use this:
   
    Example:

        //START OF FILE//
        many: true

        meteoricRuleIron:
          material: IRON_ORE
          defaultDrop: true
          itemMaps:
            - meteoricForIron
            - fluxMapIron
          compare:
            materials:
            - STONE_PICKAXE
            - IRON_PICKAXE
            - GOLDEN_PICKAXE
            - DIAMOND_PICKAXE
            - NETHERITE_PICKAXE
          deny:
            enchantments:
              SILK_TOUCH: 1
          chestPlate:
            compare:
              lore:
              - '&2The jacket came from'
              - '&2my grandfather, a welder.'
              - '&cA full outfit is required for the effect!'
          leggings:
            compare:
              lore:
              - '&2Pants repaired many times'
              - '&2after multiple sparks'
              - '&cA full outfit is required for the effect!'

        meteoricRuleIronGoldFlux:
          material: IRON_ORE
          defaultDrop: true
          itemMaps:
          - meteoricForIron
          - fluxMapGold
          compare:
            materials:
            - STONE_PICKAXE
            - IRON_PICKAXE
            - GOLDEN_PICKAXE
            - DIAMOND_PICKAXE
            - NETHERITE_PICKAXE
          deny:
            enchantments:
              SILK_TOUCH: 1
          chestPlate:
            compare:
              lore:
              - '&2The jacket came from'
              - '&2my grandfather, a welder.'
              - '&cA full outfit is required for the effect!'
          leggings:
            compare:
              lore:
              - '&2Pants repaired many times'
              - '&2after multiple sparks'
              - '&cA full outfit is required for the effect!'
          helmet:
            compare:
              lore:
              - '&2I don't even know where it is from.'
              - '&2¯\_(ツ)_/¯'
              - '&cWorks only with pants and jacket.'
              - '&cGives you a chance to find a unique gold wire'
              - '&cinstead of flux cored wire.'
        //END OF FILE//

    In example we are built a simple tree of effects. The main idea of example - that if you have the leggings
    and chestplate of your grandfather-welder, from iron ore will drop fluxIronMap and meteoricForIron item.
    But! If you will have a helmet, you will not be able to get fluxIronMap, you can get only fluxMapGold and
    meteoricForIron.
    Also, the list of rules, where you can use armor specification:
    - block break rule
    - block damage rule
    - entity damage by entity rule
    - entity shoot bow rule
    - player fish rule
    - player harvest rule
    - player shear rule
    

2. You can specify messages if some itemMap will drop some item
    -
    Quite simple. If while some rule execution some item should be dropped from itemMaps, you can specify
    message, that can be written directly to player or to all players on the server.
   
    Mandatory params:
    - messageSpec.message - message (chat color can be used with & symbol)
    - messageSpec.receiver - receiver of message (SPECIFIC_PLAYER or ALL)
    
    Example:
        
        //START OF FILE//
        many: true
        
        testWheat:
          material: WHEAT
          amount:
            max: 10
            min: 5
          sound:
            name: block.anvil.destroy
            category: MASTER
          messageSpec:
            message: '&2You are a good man!'
            receiver: SPECIFIC_PLAYER
        
        testPotato:
          material: POTATO
          amount:
            max: 20
            min: 10
          sound:
            name: block.anvil.destroy
            category: MASTER
          messageSpec:
            message: '&2Someone on server is a good man!'
            receiver: ALL
        //END OF FILE//

    In case of SPECIFIC_PLAYER, message will receive only a player, who triggered rule execution, which triggered
    dropping of an item from itemMaps. In case of ALL, message will receive all players on server.
   

3. New rule type: player harvest block rule
    -
    With that rule you can change behavior of getting sweet berries or other thing, when you don't need to break
    some block to get harvest.
   
    Mandatory params:
    - blockType - specifies type of block on which rule can be triggered (https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Material.html)

    Optional params:
    - sound.name - name of sound in resource pack or default minecraft (mandatory to specify sound)
    - sound.category - category of sound in resource pack or default minecraft (mandatory to specify sound)
    - sound.volume - volume of sound (double, by default - 100) (optional to specify sound)
    - sound.pitch - pitch of sound (double, by default - 1) (optional to specify sound)
    - itemMaps - list of itemMaps dropped when rule will be executed
    - compare.materials - list of materials of items that needed to shear entity for the rule to be executed (https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Material.html)
    - compare.lore - list of lore of items that needed to shear entity for the rule to be executed
    - compare.enchantments - list of enchants of items that needed to shear entity for the rule to be executed
    - compare.displayName - display name of items that needed to shear entity for the rule to be executed
    - deny.materials - list of materials (any) in the presence of which the rule will not be executed
    - deny.lore - list of lore strings (any) in the presence of which the rule will not be executed
    - deny.enchantments - list of enchantments (any) in the presence of which the rule will not be executed
    - deny.displayName - display name in the presence of which the rule will not be executed
    - defaultDrop - specifies to drop default items or not (default value - true)
   
    ALSO NOTICE THAT YOU CAN APPLY TO THIS RULE ARMOR FILTERS. TO NOT TO DUPLICATE INFO JUST LOOK UP
    THE FIRST FEATURE OF THIS FEATURE PACK.
   
    Example:
   
        //START OF FILE//
        many: true

        testHarvestRule:
          defaultDrop: false
          itemMaps:
          - testWheat
          - testPotato
          dropLimit: 1
          blockType: SWEET_BERRY_BUSH
        //END OF FILE//


4. In each rule where itemMaps section is present you can specify drop limitation
    -
    dropLimit param will limit max amount of different items dropped while calculating itemMaps.
    As you can see in the previous example of new harvest block rule, dropLimit is specified.
    dropLimit with the value 1 in this case means that rule will drop only one of two maps
    specified in this rule. Param can take any integer value.
   
